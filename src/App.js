import React, { Component } from 'react';
//import ReactGA from 'react-ga';
import $ from 'jquery';
import './App.css';
import Header from './Components/Header';
import Footer from './Components/Footer';
import About from './Components/About';
import Resume from './Components/Resume';
import Contact from './Components/Contact';
import Testimonials from './Components/Testimonials';
import Portfolio from './Components/Portfolio';
import Work from './Components/Work';


class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      foo: 'bar',
      resumeData: {},
      workNo: 0
    };

    //ReactGA.initialize('UA-110570651-1');
    //ReactGA.pageview(window.location.pathname);

    this.updateWorkState = this.updateWorkState.bind(this)
  }

  updateWorkState(id) {
    this.setState({
      workNo: id
    })
  }

  getResumeData(){
    $.ajax({
      url:'/resumeData.json',
      dataType:'json',
      cache: false,
      success: function(data){
        this.setState({resumeData: data});
      }.bind(this),
      error: function(xhr, status, err){
        console.log(err);
        alert(err);
      }
    });
  }

  componentDidMount(){
    this.getResumeData();
  }

  render() {
    return (
      <div className="App">
        <Header data={this.state.resumeData.main}/>
        <Portfolio data={this.state.resumeData.portfolio} updateWorkState={this.updateWorkState}/>
        <Contact data={this.state.resumeData.main}/>
        <Work data={this.state.resumeData.portfolio} workNo={this.state.workNo}/>
        <Footer data={this.state.resumeData.main} />
      </div>
    );
  }
}

export default App;
