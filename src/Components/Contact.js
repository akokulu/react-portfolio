import React, { Component } from 'react';

class Contact extends Component {
  render() {

    if(this.props.data){
      var name = this.props.data.name;
      var bio = this.props.data.bio;
      var city = this.props.data.address.city;
      var email = this.props.data.email;
      var message = this.props.data.contactmessage;
    }

    return (
      <section id="contact">

         <div className="row section-head">
            <div className="two columns header-col">
               <h1><span>Get In Touch.</span></h1>
            </div>
            <div className="ten columns">
                  <p className="lead">{message}</p>
            </div>
         </div>
         <div className="row">
           <aside className="four columns footer-widgets">
              <div className="widget widget_contact">

               <h4>About Me</h4>
               <p className="address">
                 {bio}<br />
               </p>
             </div>

           </aside>

          </div>
          <div className="close-contact"><span></span><span></span></div>
        </section>
    );
  }
}

export default Contact;
