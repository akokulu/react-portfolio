import React, { Component } from 'react';


class Work extends Component {

  render() {
    if(this.props.data){
      var title = this.props.data.projects[this.props.workNo].title;
      var category = this.props.data.projects[this.props.workNo].category;
      var image = "images/portfolio/"+ this.props.data.projects[this.props.workNo].image;
      var role = this.props.data.projects[this.props.workNo].role;
      var description = this.props.data.projects[this.props.workNo].description;
    }

    return (
      <section id="work">
        <div className="row">
           <div className="three columns">
              <img className="profile-pic" src={image} alt="Work Picture" />
           </div>
           <div className="nine columns main-col">
              <h2>{title}</h2>

              <p>{description}</p>
              <div className="row">
                 <div className="columns contact-details">
                    <h2>Role</h2>
                    <p className="address">
        						   <span>{role}</span><br />
                       {/*
        						   <span>{street}<br /> {city} {state}, {zip} </span><br />
        						   <span>{phone}</span><br />
                       <span>{email}</span>
                       */}
  		              </p>
                 </div>
              </div>
           </div>
        </div>
        <div className="close-work"><span></span><span></span></div>
     </section>
    );
  }
}

export default Work;
