import React, { Component } from 'react';

class Header extends Component {

  render() {

    if(this.props.data){
      var name = this.props.data.name;
      var occupation= this.props.data.occupation;
      var description= this.props.data.description;
      var city= this.props.data.address.city;
      var networks= this.props.data.social.map(function(network){
        return <li key={network.name}><a target="_blank" rel="noopener noreferrer" href={network.url}><i className={network.className}></i></a></li>
      })
    }
    function ShowHide()
    {

    }

    return (
      <header id="home">

        <nav id="nav-wrap">

          <a className="mobile-btn" href="#nav-wrap" title="Show navigation">Show navigation</a>
          <a className="mobile-btn" href="#home" title="Hide navigation">Hide navigation</a>

           <ul id="nav" className="nav">
              {/*<li className="current"><a className="smoothscroll" href="#home">Home</a></li>*/}
              <li id="white-logo" className="white-logo"></li>
              {/*<li><a className="smoothscroll" href="#about">About</a></li>*/}
  	          {/*<li><a className="smoothscroll" href="#resume">Resume</a></li>*/}
              <li><a className="smoothscroll" href="#portfolio">Recent Works</a></li>
              {/*<li><a className="smoothscroll" href="#testimonials">Testimonials</a></li>*/}
              {/*<li><a className="smoothscroll" href="#contact">Contact</a></li>*/}
           </ul>

        </nav>
        <div className="row banner">
           <div className="banner-text">
              <h1 className="responsive-headline">{name}</h1>
              {/*<h3>I'm a {city} based <span>{occupation}</span>. {description}.</h3>*/}
              <h3>
                <div className="glitch" data-text="Toronto based">{city} based </div>
                {/*<div className="glitch" data-text="Full-Stack"> {description} </div>*/}
                {/*<div className="glitch" data-text="Creative"> {description} </div>*/}
                <span><div className="glitch" data-text="Developer"> {occupation} </div></span>
              </h3>
              <hr />
              {/*
              <div id="aboutme">
                <svg version="1.1" className="goo">
                  <defs>
                    <filter id="goo">
                      <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                      <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                      <feComposite in="SourceGraphic" in2="goo"/>
                    </filter>
                  </defs>
                </svg>

                <span className="button--bubble__container">
                  <a href="javascript:void(0);" className="button button--bubble">
                    About Me
                  </a>
                  <span className="button--bubble__effect-container">
                    <span className="circle top-left"></span>
                    <span className="circle top-left"></span>
                    <span className="circle top-left"></span>
                    <span className="button effect-button"></span>
                    <span className="circle bottom-right"></span>
                    <span className="circle bottom-right"></span>
                    <span className="circle bottom-right"></span>
                  </span>
                </span>
              </div>
              */}
              <ul className="social">
                 {networks}
              </ul>
           </div>
        </div>
        <div className="logo"></div>
        <div className="pipe-blink"></div>

        <p className="scrolldown">
           <a className="smoothscroll" href="#portfolio"><i className="icon-down-circle"></i></a>
        </p>
     </header>
    );
  }
}

export default Header;
